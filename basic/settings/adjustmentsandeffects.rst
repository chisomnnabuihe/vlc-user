####################
Adjustment & Effects
####################

VLC allows you to add adjustments and effects to the playing Video or Audio file.
 
*************
Audio Effects
*************

Equalizer
=========

VLC features a 10-band graphical equalizer, a device used to alter the relative frequencies of audio (e.g. for a bass boost). 
You can display it by selecting :menuselection:`Tools --> Effects and Filters --> Audio Effects --> Equalizer` on Windows or GNU/Linux or by clicking the :guilabel:`Equalizer` button on the macOS interface. 

.. figure::  /images/basic/settings/adjustmentsandeffects_audio.png
   :align:   center

   Audio equalizer in the Windows and GNU/Linux interface.

.. Hint:: Do not forget to check the :guilabel:`Enable` button to activate the Equalizer!

Additional Audio Effects
========================

From the :guilabel:`Audio Effects` menu, you can also access the following effects:

Compressor
   emulates a dynamic range compressor
Spatializer
   changes audio as if it was produced inside a room
Stereo Widener
   enhances the stereo effect by suppressing mono (signal common to both channels) and by delaying the signal of left into right and vice versa, thereby widening the stereo effect.
Advanced
   From this menu, you can adjust the audio pitch.

*************
Video Effects
*************

VLC features several filters able to change the video's distortion, brightness adjustment, motion blurring, etc. 

.. figure::  /images/basic/settings/adjustmentsandeffects_video.png
   :align:   center

***************
Synchronization
***************

Synchronizing Audio and Video subtitle track is possible in VLC. 

.. figure::  /images/basic/settings/adjustmentsandeffects_synchronization.png
   :align:   center
