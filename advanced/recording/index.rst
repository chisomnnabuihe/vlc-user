#####################################
 How to Record Audio or Video in VLC
#####################################

.. toctree::
   :maxdepth: 3
   :name: toc-recording
   
   audio_recording.rst
   video_recording.rst
   