#############################
 How to Record Audio in VLC
#############################


1. Select :menuselection:`Media --> Open capture Device` or press :guilabel:`'Ctrl+C'`, then under capture mode click the **Dropdown menu** and select :guilabel:`DirectShow`.
  
.. figure::  /images/advanced/recording/select_directshow.png
   :align:   center
   
   **Note: If you want to change the audio recording device, click the dropdown menu under audio device name and select your desire device** 

.. figure::  /images/advanced/recording/change_audio_device_name.png
   :align:   center  
   
2. Click the arrow attached to the play button then select convert or press :guilabel:`'Alt+O'`.

.. figure::  /images/advanced/recording/click_convert_2.png
   :align:   center
3. Under profile click the dropdown to select your desired audio output format.

.. figure::  /images/advanced/recording/output_format_audio.png
   :align:   center
   
4. Click browse to select a save location.

.. figure::  /images/advanced/recording/set_destination_audio.png
   :align:   center
   
6. Click start to commence the recording.

.. figure::  /images/advanced/recording/start_recording_audio.png
   :align:   center
   
7. Click the stop button |stop| to stop recording. Your audio will be stored in the destination you chose.

.. |stop| image:: /images/advanced/recording/stop.png
   :align: middle
   :width: 12
