#############################
 How to Record Videos in VLC
#############################

VLC provides a variety of options for recording videos. You can make a video recording of a movie/video you are watching, record your desktop or even record yourself using a webcam or a similar input device. These recordings can also be saved in a variety of formats.

*****************************
Recording from videos/movies
*****************************
There are two ways to record content being viewed in VLC, whichever way you choose you need to set preferences for where you want the video to be saved. To do this:

* Select :menuselection:`Tools --> Preferences` or press :guilabel:`'Ctrl+P'`.

.. figure::  /images/advanced/recording/set_preference.png
   :align:   center

* Click :guilabel:`Input/Codecs`, under **Files** click :guilabel:`Browse...` to select a location for your recordings.

.. figure::  /images/advanced/recording/set_location.png
   :align:   center

.. figure::  /images/advanced/recording/select_folder.png
   :align:   center
   
* Click :guilabel:`Save`

.. figure::  /images/advanced/recording/save.png
   :align:   center

**To record the video you can:**

1. Select :menuselection:`Playback --> Record` to start the recording, you can also repeat the process to stop the recording or click the |stop| button.

.. |stop| image:: /images/advanced/recording/stop.png
   :align: middle
   :width: 12

.. figure::  /images/advanced/recording/record.png
   :align:   center

2. Select :menuselection:`View --> Advanced controls`, click the :guilabel:`Record` button |record|. Click the button again to stop the recording.

.. figure::  /images/advanced/recording/advanced_controls.png
   :align:   center
   
.. |record| image:: /images/advanced/recording/record_button.png
   :align: middle
   :width: 12

**********************
Recording your Desktop
**********************

1. Select :menuselection:`Media --> Open capture Device` or press :guilabel:`'Ctrl+C'`, then under capture mode click the **Dropdown menu** and select :guilabel:`Desktop`.

.. figure::  /images/advanced/recording/open_capture_device.png
   :align:   center

.. figure::  /images/advanced/recording/select_desktop.png
   :align:   center
   
2. Under options set your desired frame rate.

.. figure::  /images/advanced/recording/set_framerate.png
   :align:   center
   
3. Click the arrow attached to the play button then select convert or press :guilabel:`'Alt+O'`.

.. figure::  /images/advanced/recording/click_convert.png
   :align:   center
4. Under profile click the dropdown to select your desired output format.

.. figure::  /images/advanced/recording/output_format.png
   :align:   center
   
5. Click browse to select a save location.

.. figure::  /images/advanced/recording/set_destination.png
   :align:   center
   
6. Click start to commence the recording.

.. figure::  /images/advanced/recording/start_recording.png
   :align:   center

7. Click the stop button |stop| to stop recording. Your video will be stored in the destination you chose.

****************************
Recording Videos with Webcam
****************************

1. Select :menuselection:`Media --> Open capture Device` or press :guilabel:`'Ctrl+C'`, then under capture mode click the **Dropdown menu** and select :guilabel:`DirectShow`.
  
.. figure::  /images/advanced/recording/select_directshow.png
   :align:   center
   
2. Click the arrow attached to the play button then select convert or press :guilabel:`'Alt+O'`.

.. figure::  /images/advanced/recording/click_convert_2.png
   :align:   center
3. Under profile click the dropdown to select your desired output format.

.. figure::  /images/advanced/recording/output_format_2.png
   :align:   center
   
4. Click browse to select a save location.

.. figure::  /images/advanced/recording/set_destination_2.png
   :align:   center
   
6. Click start to commence the recording.

.. figure::  /images/advanced/recording/start_recording_2.png
   :align:   center
   
7. Click the stop button |stop| to stop recording. Your video will be stored in the destination you chose.